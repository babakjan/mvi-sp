# Generování obrázků kámen nůžky papír

## Zadání

Natrénujte neuronovou síť, která bude schopná generovat obrázky gest ruky
ze hry kámen-nůžky-papír.

## Spuštění

-   Nejjednodušší spuštění - otevření jupyter notebooků v Google colab. Notebooky u mě na google [drive](https://drive.google.com/drive/folders/1bkY2WwkQ_pDUwYAv_YCOsoIIW8aX9aq_?usp=sharing).

-   Druhá možnost stažení notebooků odsud z Gitlabu [/jupyter-notebooks](./jupyter-notebooks/)

-   Je důležité změnit cestu k trénovacímu datasetu, který si lze stáhnout ode mne z google [disku](https://drive.google.com/drive/folders/10z36IiuhsGmAo6sm1I0ryCSTgv-sjGVG?usp=sharing).
    ```python
    DATA_DIR = '/content/drive/MyDrive/Developer/MVI/rock_paper-scissors/data'
    ```

 ## Report

 [report.pdf](./report.pdf)

 ## Videa

 Vytvořil jsem [videa](./videos/) zobrazující průběh trénování neuronových sítí.